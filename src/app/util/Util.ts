import * as moment from 'moment';

export default class Util {
    static parseStringToDateUsingSpecificFormat(dateString: string):Date {
        const parsedDate = moment(dateString, 'YYYY-M-D h:maaaaa');
        return parsedDate.toDate();
    }
}