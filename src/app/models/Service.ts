import { ServiceType } from './ServiceType';

export class Service {
    serviceType: ServiceType
    hours: number; // TODO: use BigNumber as hours can be fractional and we are using it for a currency calc.
}