import { ItemType } from './ItemType';

export class SoldItem {
    itemType: ItemType;
    units: number;
}