import { SoldItem } from './SoldItem';
import { Freebie } from './Freebie';

export class ItemsSold {
    items: SoldItem[];
    freebies: Freebie[];
}