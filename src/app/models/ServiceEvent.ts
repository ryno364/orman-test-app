import { Service } from './Service';
import { ItemsSold } from './ItemsSold';

export class ServiceEvent {
    id: number;
    date: string;
    servicesPerformed: Service[];
    itemsSold: ItemsSold;
}