export class OdometerReading {
    miles: number;
    date: string;
}