import { FreebieType } from './FreebieType';

export class Freebie {
    fType: FreebieType;
    count: number;
}