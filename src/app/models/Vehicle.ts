import { ServiceEvent } from './ServiceEvent';
import { OdometerReading } from './OdometerReading';

export class Vehicle {

    name: string;
    type: string;
    created: string;
    horsepower: number;

    serviceEvents: ServiceEvent[];
    odometerReadings: OdometerReading[];
}