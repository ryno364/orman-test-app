import { Injectable } from '@angular/core';
import { Vehicle } from '../models/Vehicle';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  private url = 'https://api.myjson.com/bins/11melk';

  

  selectedVehicle: Vehicle;

  constructor(private http: HttpClient) {

  }

  getVehicles(): Observable<VehicleGetResponse> {
    return this.http.get<VehicleGetResponse>(this.url)
    .pipe(
      catchError(this.handleError<VehicleGetResponse>('getVehicles', null))
    );
  }

  
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console

      // TODO: We should use something better then just an alert.
      alert(`Error retrieving data: ${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }
}

export class VehicleGetResponse {
  vehicles: Vehicle[];
  constructor(vehicles: Vehicle[]) {
    this.vehicles = vehicles;
  }
}