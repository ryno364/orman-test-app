import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ServiceEvent } from '../../models/ServiceEvent';
import Util from '../../util/Util';

@Component({
  selector: 'app-service-events',
  templateUrl: './service-events.component.html',
  styleUrls: ['./service-events.component.scss']
})
export class ServiceEventsComponent implements OnInit {

  @Input() serviceEventsForVehicle: ServiceEvent[];
  
  // We want to tell the parent component which ServiceEvent's ItemsSold to navigate to.
  @Output() itemIndexToNavigateToEvent = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {

  }

  parseDate(dateString: string):Date {
    let dateObj = Util.parseStringToDateUsingSpecificFormat(dateString);
    return dateObj;
  }

  onItemsSoldSelected(indexOfItemsSold: number) {
    this.itemIndexToNavigateToEvent.emit(indexOfItemsSold);
  }

  titleCase(string: string) {
    return string.toLowerCase().split(' ').map(function(word) {
      return (word.charAt(0).toUpperCase() + word.slice(1));
    }).join(' ');
  }

}
