import { Component, OnInit, Input } from '@angular/core';
import { ServiceEvent } from '../../models/ServiceEvent';
import Util from '../../util/Util';

@Component({
  selector: 'app-items-sold',
  templateUrl: './items-sold.component.html',
  styleUrls: ['./items-sold.component.scss']
})
export class ItemsSoldComponent implements OnInit {

  @Input() serviceEventsForVehicle: ServiceEvent[];
  @Input() serviceEventsIndexToView: number;

  constructor() { }

  ngOnInit() {

  }

  parseDate(dateString: string):Date {
    let dateObj = Util.parseStringToDateUsingSpecificFormat(dateString);
    return dateObj;
  }

  titleCase(string: string) {
    return string.toLowerCase().split(' ').map(function(word) {
      return (word.charAt(0).toUpperCase() + word.slice(1));
    }).join(' ');
  }

}
