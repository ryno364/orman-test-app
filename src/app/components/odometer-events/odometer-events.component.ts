import { Component, OnInit, Input } from '@angular/core';
import { OdometerReading } from '../../models/OdometerReading';
import Util from '../../util/Util';

@Component({
  selector: 'app-odometer-events',
  templateUrl: './odometer-events.component.html',
  styleUrls: ['./odometer-events.component.scss']
})
export class OdometerEventsComponent implements OnInit {

  @Input() odometerReadingsForVehicle: OdometerReading[];

  constructor() { }

  ngOnInit() {
    
  }

  parseDate(dateString: string):Date {
    let dateObj = Util.parseStringToDateUsingSpecificFormat(dateString);
    return dateObj;
  }

}
