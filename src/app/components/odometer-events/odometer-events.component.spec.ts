import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OdometerEventsComponent } from './odometer-events.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('OdometerEventsComponent', () => {
  let component: OdometerEventsComponent;
  let fixture: ComponentFixture<OdometerEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ], // TODO: Remove NO_ERRORS_SCHEMA so we can fully test the app.
      imports: [ HttpClientTestingModule ],
      declarations: [ OdometerEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OdometerEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test date parsing.', () => {
    const date = component.parseDate("2019-11-23 5:30a")
    expect(date).toEqual(new Date("Nov 23, 2019, 5:30:00 AM"))
  });

});
