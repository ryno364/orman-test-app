import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Vehicle } from '../../models/Vehicle';
import { VehicleService } from '../../web_services/vehicle.service';
import { MatTabGroup } from '@angular/material';
import Util from '../../util/Util';

@Component({
  selector: 'app-vehicle-detail',
  templateUrl: './vehicle-detail.component.html',
  styleUrls: ['./vehicle-detail.component.scss',]
})
export class VehicleDetailComponent implements OnInit {

  @Input() vehicle: Vehicle;

  @ViewChild('tabs', { static: false }) tabGroup: MatTabGroup;

  serviceEventIndexToNavigateTo: number;

  constructor(private vehicleService: VehicleService) {
      
  }

  ngOnInit() {
    
  }

  parseDate(dateString: string):Date {
    let dateObj = Util.parseStringToDateUsingSpecificFormat(dateString);
    return dateObj;
  }

  navigateToItemsSold(event) {
    // Set the tab to the Items Sold tab.
    this.tabGroup.selectedIndex = 2;
    // In the items sold tab, navigate to the service event the user chose.  
    // So they can view items for this service event.
    this.serviceEventIndexToNavigateTo = event;
  }
  
}
