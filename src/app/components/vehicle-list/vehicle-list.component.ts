import { Component, OnInit, ViewChild } from '@angular/core';
import { Vehicle } from '../../models/Vehicle';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { VehicleService } from '../../web_services/vehicle.service';

@Component({
  selector: 'app-vehicle-list',
  templateUrl: './vehicle-list.component.html',
  styleUrls: ['./vehicle-list.component.scss']
})
export class VehicleListComponent implements OnInit {
  
  selectedVehicle: Vehicle;
  displayedColumns: string[] = ['name', 'type'];
  dataSource: MatTableDataSource<Vehicle>;

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private vehicleService: VehicleService) {
     
  }

  onRowSelect($event, currVehicle) {
    this.vehicleService.selectedVehicle = currVehicle;
    this.selectedVehicle = currVehicle;
  }

  ngOnInit() {
    this.vehicleService.getVehicles().subscribe(result => {
      this.dataSource = new MatTableDataSource(result.vehicles);
      this.dataSource.sort = this.sort;
    });
  }
}
